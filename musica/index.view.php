<?php
    include 'partials/header.part.php';
?>
<form action="<?= $_SERVER['PHP_SELF']; ?>" method="post">
    <div>
        <label for="texto">Texto a buscar</label>
        <input type="text" name="texto" id="texto">
    </div>
    <div>
        <label>Buscar en</label>
        <input type="radio" name="buscaren" value="titulo" id="titulo">
        <label for="titulo">Título</label>
        <input type="radio" name="buscaren" value="album" id="album">
        <label for="album">Album</label>
        <input type="radio" name="buscaren" value="ambos" id="ambos">
        <label for="ambos">Ambos</label>
    </div>
    <div>
        <label for="genero">Género</label>
        <select name="genero" id="genero">
            <option value="todos">Todos</option>
            <option value="blues">Blues</option>
            <option value="jazz">Jazz</option>
            <option value="pop">Pop</option>
            <option value="rock">Rock</option>
        </select>
    </div>

    <input type="submit" name="enviar" value="Buscar">
</form>

<table>
    <tr>
        <th>Título</th>
        <th>Álbum</th>
        <th>Género</th>
    </tr>
    <?php foreach($canciones as $cancion) : ?>
        <tr>
            <td><?= $cancion['titulo']; ?></td>
            <td><?= $cancion['album']; ?></td>
            <td><?= $cancion['genero']; ?></td>
        </tr>
    <?php endforeach; ?>
</table>

<?php
    include 'partials/footer.part.php';
?>
