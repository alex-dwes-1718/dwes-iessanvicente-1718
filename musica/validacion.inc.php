<?php

function buscaEnString(
    array $cancion, string $texto, string $nombreCampo) : bool
{
    if (strpos($cancion[$nombreCampo], $texto) !== false)
        return true;

    return false;
}

function buscaEnTitulo(
    array $cancion, string $texto) : bool
{
    return buscaEnString($cancion, $texto, 'titulo');
}

function buscaEnAlbum(
    array $cancion, string $texto) : bool
{
    return buscaEnString($cancion, $texto, 'album');
}

function buscaEnTituloOAlbum(
    array $cancion, string $texto) : bool
{
    return (buscaEnString($cancion, $texto, 'album')
        ||
        buscaEnString($cancion, $texto, 'titulo'));
}

function buscaTextoEnCancion($buscaren, $cancion, $texto)
{
    $encontrado = false;

    switch($buscaren)
    {
        case 'titulo':
            $encontrado = buscaEnTitulo($cancion, $texto);
            break;
        case 'album':
            $encontrado = buscaEnAlbum($cancion, $texto);
            break;
        case 'ambos':
            $encontrado = buscaEnTituloOAlbum($cancion, $texto);
            break;
        default:
            $encontrado = false;
            break;
    }

    return $encontrado;
}

function filtraFormulario(array $canciones) : array
{
    $texto = $_POST['texto'] ?? '';
    $buscaren = $_POST['buscaren'] ?? 'ambos';
    $genero = $_POST['genero'] ?? '';

    $fnFiltra = function (array $cancion)
    use ($texto, $buscaren, $genero)
    {
        $encontrado = false;
        if (!empty($texto))
        {
            $encontrado = buscaTextoEnCancion($buscaren, $cancion, $texto);
        }
        if (empty($texto) || $encontrado === true)
        {
            if ($genero === 'todos' || $genero === $cancion['genero'])
                return true;
        }

        return false;
    };

    return array_filter($canciones, $fnFiltra);
}