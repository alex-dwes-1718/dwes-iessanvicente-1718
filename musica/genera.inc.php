<?php

function generaVistaCanciones(array $canciones)
{
    $fnGeneraStringsCanciones = function (array $cancion)
    {
        $stCancion = '<tr><td>';
        $stCancion .= $stCancion['titulo'];
        $stCancion .= '</td><td>';
        $stCancion .= $stCancion['album'];
        $stCancion .= '</td><td>';
        $stCancion .= $stCancion['genero'];
        $stCancion .= '</td></tr>';

        return $stCancion;
    };

    return array_map(
        $fnGeneraStringsCanciones, $canciones);
}