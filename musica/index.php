<?php
require 'validacion.inc.php';

$cancionesOriginales = require 'canciones.inc.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST')
    $canciones = filtraFormulario($cancionesOriginales);
else
    $canciones = $cancionesOriginales;

include 'index.view.php';