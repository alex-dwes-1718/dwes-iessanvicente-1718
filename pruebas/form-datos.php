<?php
    require 'form-validacion.inc.php';

    include 'header.part.php';
?>

<h1>Formulario</h1>
<form action="<?= $_SERVER['PHP_SELF']; ?>" method="post" novalidate>
    <label for="fechaNacimiento">Fecha de nacimiento</label>
    <input type="text" name="fechaNacimiento"
           class="<?= getClassRequiredField('fechaNacimiento', 'filterDate'); ?>"
           value="<?= $_POST['fechaNacimiento'] ?? ''; ?>">
    <label for="email">Correo electrónico</label>
    <input
            type="email" name="email"
            class="<?= getClassRequiredField('email', 'filterEmail'); ?>"
            value="<?= $_POST['email'] ?? ''; ?>">
    <label for="observaciones">Observaciones</label>
    <textarea name="observaciones"
              class="<?= getClassRequiredField('observaciones'); ?>"><?= $_POST['observaciones'] ?? ''; ?></textarea>
    <input type="submit" value="Enviar">
</form>

<?php
    filterForm();

    include 'footer.part.php';
?>

