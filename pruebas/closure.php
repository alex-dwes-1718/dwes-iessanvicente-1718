<?php
function saluda(callable $fnSaludo, $nombre)
{
    echo '<header>';
    $fnSaludo($nombre);
    echo '</header>';
}

$saludaH1 = function ($nombre)
{
    echo "<h1>Hola {$nombre}</h1>";
};

saluda($saludaH1, 'Pedro');

$saludaP = function ($nombre)
{
    echo "<p>Hola {$nombre}</p>";
};

saluda(
    function ($nombre)
    {
        echo "<p>Hola {$nombre}</p>";
    },
    'Pedro');