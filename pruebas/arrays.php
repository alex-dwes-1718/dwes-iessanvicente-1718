<?php
    $alumnos = [
        [
            'id'=>1,
            'nombre'=>'Irene',
            'edad'=>18
        ],
        [
            'id'=>2,
            'nombre'=>'Juanjo',
            'edad'=>22
        ],
        [
            'id'=>3,
            'nombre'=>'David',
            'edad'=>25
        ]
    ];
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Alumnos</title>
</head>
<body>

<h1>Alumnos</h1>
<?php
    if (count($alumnos) > 0) {
?>
        <table>
            <tr>
                <?php
                $claves_alumno = array_keys($alumnos[0]);
                foreach ($claves_alumno as $clave)
                    echo '<th>' . strtoupper($clave) . '</th>';
                ?>
            </tr>
            <?php
            foreach ($alumnos as $alumno) {
                echo '<tr>';

                foreach ($alumno as $dato)
                    echo '<td>' . $dato . '</td>';

                echo '</tr>';
            }
            ?>
        </table>
<?php
    }
?>
</body>
</html>

