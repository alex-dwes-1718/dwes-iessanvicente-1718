<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?= $nombre; ?></title>
</head>
<body>

<h1>Formulario</h1>
<form action="<?= $_SERVER['PHP_SELF']; ?>" method="post">
    <label for="nombre">Nombre</label>
    <input type="text" name="nombre" value="">
    <br>
    <label for="apellido">Apellido</label>
    <input type="text" name="apellido" value="">
    <br>
    <input type="submit" value="Enviar">
</form>
<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        echo $_POST['nombre'] . '<br>';
        echo $_POST['apellido'];
    }
?>
</body>
</html>
