<?php
    $alumnos = array(
        array('nombre'=>'Irene', 'edad' =>25),
        array('nombre'=>'Pedro', 'edad' =>15),
        array('nombre'=>'Juan', 'edad' =>30),
        array('nombre'=>'Álex', 'edad' =>14),
    );

    $tipoSalida = $_GET['format'] ?? 'html';
    if ($tipoSalida === 'json')
        include 'index.view.json.php';
    else
        include 'index.view.php';
?>