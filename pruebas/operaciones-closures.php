<?php

function calcula(callable $operacion, float $op1, float $op2)
{
    $arrR = $operacion($op1, $op2);

    echo
    "{$op1} {$arrR['simbolo']} {$op2} = {$arrR['resultado']}";
}

$suma = function (float $op1, float $op2)
{
    return [
        'resultado' => $op1 + $op2,
        'simbolo' => '+'
    ];
};

calcula($suma, 5, 3);