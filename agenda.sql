-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 29-11-2017 a las 18:46:59
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `agenda`
--
CREATE DATABASE IF NOT EXISTS `agenda` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci;
USE `agenda`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contactos`
--

CREATE TABLE `contactos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `telefono` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `fecha_alta` datetime NOT NULL,
  `foto` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `grupo` int(11) NOT NULL,
  `usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `contactos`
--

INSERT INTO `contactos` (`id`, `nombre`, `telefono`, `fecha_alta`, `foto`, `grupo`, `usuario`) VALUES
(4, 'pepe', 'dghdf', '0000-00-00 00:00:00', './uploads/1510938749_descarga.jpg', 1, 1),
(7, 'pepa', 'pepe', '0000-00-00 00:00:00', './uploads/1511290485_VW-Golf-R-6.jpg', 1, 1),
(8, 'pepa', 'pepe', '0000-00-00 00:00:00', '/uploads/anonimo.png', 1, 2),
(9, 'coco', 'coco', '0000-00-00 00:00:00', './uploads/1511291722_maxresdefault.jpg', 1, 1),
(10, 'qwqwq', 'qwqw', '0000-00-00 00:00:00', 'uploads/1511291864_maxresdefault.jpg', 1, 1),
(12, 'coco', 'cocoqqq', '0000-00-00 00:00:00', '/uploads/anonimo.png', 1, 3),
(13, '', '', '0000-00-00 00:00:00', '/uploads/anonimo.png', 1, 1),
(14, 'qqqq', 'qqq', '0000-00-00 00:00:00', '/uploads/anonimo.png', 1, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupos`
--

CREATE TABLE `grupos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `grupos`
--

INSERT INTO `grupos` (`id`, `nombre`) VALUES
(1, 'ALUMNOS'),
(2, 'PROFESORES');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_spanish2_ci NOT NULL,
  `role` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `salt` varchar(22) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `password`, `role`, `salt`) VALUES
(1, 'dwes', 'dwes', 'ROLE_ADMIN', ''),
(3, 'pepe', 'pepe', 'ROLE_USER', ''),
(8, 'meme', 'meme', 'ROLE_USER', ''),
(11, 'tete', 'tete', 'ROLE_USER', ''),
(12, 'titi', 'titi', 'ROLE_USER', ''),
(14, 'tito', 'tito', 'ROLE_USER', ''),
(15, 'popo', '$2y$05$wo0IDcZx70q.pFM5azm.Zu1.8gNTyNXBgR3ik9GnHCIsWWyU/r1N2', 'ROLE_ADMIN', 'wo0IDcZx70q.pFM5azm.Zv');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `contactos`
--
ALTER TABLE `contactos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQUE_USUARIO_NOMBRE` (`usuario`,`nombre`);

--
-- Indices de la tabla `grupos`
--
ALTER TABLE `grupos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nombre` (`nombre`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `contactos`
--
ALTER TABLE `contactos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `grupos`
--
ALTER TABLE `grupos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
