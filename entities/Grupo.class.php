<?php


class Grupo
{
    private $id;
    private $nombre;
    private $numContactos;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return class
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     * @return class
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumContactos()
    {
        return $this->numContactos;
    }

    /**
     * @param mixed $numContactos
     * @return class
     */
    public function setNumContactos($numContactos)
    {
        $this->numContactos = $numContactos;
        return $this;
    }

    public function __toString()
    {
        return $this->getNombre();
    }


}