<?php


class QueryBuilder
{
    /**
     * @var PDO
     */
    private $pdo;

    /**
     * QueryBuilder constructor.
     * @param PDO $pdo
     */
    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    public function getPDO()
    {
        return $this->pdo;
    }

    public function findAll(string $table, string $classEntity) : array
    {
        $sql = "SELECT * FROM $table";
        $pdoStatement = $this->pdo->prepare($sql);
        $res = $pdoStatement->execute();

        if ($res === FALSE)
            throw new Exception('No se ha podido ejecutar la query ');

        return $pdoStatement->fetchAll(
            PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
            $classEntity);
    }
}