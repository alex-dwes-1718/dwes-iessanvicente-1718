<?php


class Connection
{
    public static function make()
    {
        try
        {
            $opciones = array(
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_PERSISTENT => true
            );

            $pdo = new PDO(
                'mysql:host=dwes.local;dbname=agenda;charset=utf8',
                'usuAgenda',
                'dwes',
                $opciones);
        }
        catch(PDOException $pdoException)
        {
            die ($pdoException->getMessage());
        }

        return $pdo;
    }
}