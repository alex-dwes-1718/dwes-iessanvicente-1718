<?php

require_once __DIR__ . '/entities/contacto.class.php';
require_once __DIR__ . '/entities/Grupo.class.php';
require_once __DIR__ . '/database/QueryBuilder.class.php';

class Agenda
{
    private static function executeQuery(
        PDO $pdo, $sql, $parameters=[], $nombreClase='Contacto', $isSelect=true)
    {
        $pdoStatement = $pdo->prepare($sql);
        $res = $pdoStatement->execute($parameters);

        if ($res === FALSE)
            throw new Exception('No se ha podido ejecutar la query ');

        if ($isSelect === true)
        {
            return $pdoStatement->fetchAll(
                PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE,
                $nombreClase);
        }

        return $res;
    }

    public static function getContactos(QueryBuilder $qb)
    {
        $contactos = $qb->findAll('contactos', 'Contacto');

        /**
         * @var Contacto $contacto
         */
        foreach ($contactos as $contacto)
        {
            $grupo = self::findGrupoById($qb->getPDO(), $contacto->getGrupo());
            $contacto->setGrupo($grupo);
        }

        return $contactos;
    }

    public static function getGrupos(QueryBuilder $qb)
    {
        return $qb->findAll('grupos', 'Grupo');
    }

    public static function addContacto(PDO $pdo, Contacto $contacto)
    {
        try
        {
            $pdo->beginTransaction();

            $sql = "INSERT INTO contactos (nombre, telefono, foto, usuario, grupo) VALUES (:nombre, :telefono, :foto, 1, :grupo)";
            self::executeQuery($pdo, $sql, [
                ':nombre'=>$contacto->getNombre(),
                ':telefono'=>$contacto->getTelefono(),
                ':foto'=>$contacto->getFoto(),
                ':grupo'=>$contacto->getGrupo()->getId(),
            ], 'Contacto', $isSelect=false);

            $sql = "UPDATE grupos SET numContactos=numContactos+1 WHERE id=:id";
            self::executeQuery(
                $pdo, $sql,
                [
                    ':id'=>$contacto->getGrupo()->getId()
                ], 'Grupo', $isSelect=false);

            $pdo->commit();
        }
        catch(PDOException $pdoEx)
        {
            $pdo->rollBack();
            throw $pdoEx;
        }
    }

    public static function deleteContacto(PDO $pdo, string $id)
    {
        $sql = "DELETE FROM contactos WHERE id=:id";
        self::executeQuery($pdo, $sql, [':id'=>$id], 'Contacto', $isSelect=false);
    }

    public static function findByNombre(PDO $pdo, string $nombre)
    {
        $sql = "SELECT * FROM contactos where nombre like :nombre";
        return self::executeQuery($pdo, $sql, [
            ':nombre'=> "%{$nombre}%"
        ]);
    }

    public static function findGrupoById(PDO $pdo, $id) : Grupo
    {
        $sql = "SELECT * FROM grupos where id=:id";
        $grupo = self::executeQuery($pdo, $sql, [
            ':id'=> $id
        ], 'Grupo');

        return $grupo[0];
    }

    public function __toString()
    {
        $strAgenda = '<p>Agenda</p>';

        foreach ($this->contactos as $contacto)
            $strAgenda .= $contacto
    . '<a href="vercontacto.php?id=' . $contacto->getId() . '">Ver contacto</a><br>';

        return $strAgenda;
    }
}