
window.addEventListener('load', inicia);

function elimina(event)
{
    event.preventDefault();
    var enlaceEliminar = this;

    swal({
        title: '¿Seguro que quieres eliminar el item?',
        text: "esta operación no podrá deshacerse",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí, bórralo!'
    }).then(function () {
        var xhttpRequest = new XMLHttpRequest();
        var url = enlaceEliminar.href;
        xhttpRequest.open('DELETE', url, true);
        xhttpRequest.send();
        xhttpRequest.onreadystatechange = function ()
        {
            if (this.status === 200 && this.readyState === 4)
            {
                var respuesta = JSON.parse(xhttpRequest.response);

                if (respuesta !== null)
                {
                    var mensaje = '';
                    if (respuesta[0].code == 200)
                    {
                        enlaceEliminar.parentNode.parentNode.remove();

                        swal(
                            'Eliminado!',
                            respuesta[0].message,
                            'success'
                        )
                    }
                    else
                        swal(
                            'No se ha elimiado!',
                            respuesta[0].message,
                            'error'
                        )
                }
            }
        };
    })
}

function inicia()
{
    var enlacesEliminar = document.getElementsByClassName('enlace-eliminar');

    for (var i = 0;i < enlacesEliminar.length;i++)
    {
        enlacesEliminar[i].addEventListener('click', elimina);
    }
}