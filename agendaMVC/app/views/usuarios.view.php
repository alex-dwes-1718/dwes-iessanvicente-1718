<h2><?= gettext("Usuarios") ?></h2>

<section id="search">
    <form id="form-busqueda" action="/usuarios" method="POST">
        <input type="search" name="busqueda" value="<?= isset($busqueda) ? $busqueda : '' ?>">
        <input type="submit" name="enviar" value="enviar">
    </form>
</section>

<nav>
    <ul><li><a href="/usuarios/new">Crear usuario</a></li></ul>
</nav>

<table>
    <tr>
        <th>Id</th>
        <th>Nombre</th>
        <th>Operaciones</th>
    </tr>
    <?php foreach($users as $user) : ?>
        <tr>
            <td><?= $user->getId(); ?></td>
            <td><?= $user->getNombre(); ?></td>
            <td>
                <a href="/usuarios/<?= $user->getId() ?>/edit">Editar</a>
                <a href="/usuarios/<?= $user->getId() ?>">Show</a>
                <a id="enlace-eliminar-<?= $user->getId() ?>" class="enlace-eliminar" href="/usuarios/<?= $user->getId() ?>">Eliminar</a>
            </td>
        </tr>
    <?php endforeach; ?>
</table>

<script src="js/crud.js"></script>
