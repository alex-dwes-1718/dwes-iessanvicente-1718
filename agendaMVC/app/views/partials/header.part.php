<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Agenda de contactos</title>
    <link type="text/css" rel="stylesheet" href="css/estilos.css">
</head>
<body>
    <header>
        <h1>Agenda de contactos</h1>
        <ul>
            <li><a href="/idiomas/en">English</a></li>
            <li><a href="/idiomas/es">Español</a></li>
        </ul>
    </header>
    <?php include __DIR__ . '/menu.part.php' ?>