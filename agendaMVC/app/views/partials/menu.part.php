<nav id="menu-principal">
    <ul>
        <?php if (!is_null($usuario)) : ?>
            <li><a href="/logout"><?= $usuario->getNombre() ?> - salir</a></li>
            <li><a href="/">Contactos</a></li>
            <?php if ($usuario->getRole() === 'ROLE_ADMIN') : ?>
                <li><a href="/usuarios">Usuarios</a></li>
            <?php endif; ?>
        <?php else : ?>
            <li><a href="/login">Login</a></li>
        <?php endif; ?>
        <li><a href="/about">Acerca de</a></li>
    </ul>
</nav>