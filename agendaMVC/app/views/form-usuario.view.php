<?php if (isset($error)) : ?>
    <div>
        <?= $error; ?>
    </div>
<?php endif; ?>
<form action="<?= isset($user) ?
    '/usuarios/' . $user->getId() . '/update'
    :
    '/usuarios/create' ?>"
      method="post" enctype="multipart/form-data">
    <div>
        <label for="nombre">Nombre</label>
        <input type="text" name="nombre" id="nombre"
               value="<?= isset($user) ? $user->getNombre() : '' ?>">
    </div>
    <div>
        <label for="password">Password</label>
        <input type="password" name="password" id="password"
               value="<?= isset($user) ? $user->getPassword() : '' ?>">
    </div>

    <input type="submit" name="enviar" value="Enviar">
</form>