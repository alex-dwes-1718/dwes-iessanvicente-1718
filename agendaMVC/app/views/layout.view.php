<?php
include 'partials/header.part.php';
?>

<?php if (isset($error_message)) : ?>
    <div>
        <?= $error_message; ?>
    </div>
<?php endif; ?>

<main>
    <?= $mainContent; ?>
</main>
<?php
include 'partials/footer.part.php';
?>
