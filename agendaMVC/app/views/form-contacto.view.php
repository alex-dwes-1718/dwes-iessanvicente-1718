<?php if (isset($error)) : ?>
    <div>
        <?= $error; ?>
    </div>
<?php endif; ?>
<form action="<?= isset($contacto) ?
    '/contactos/' . $contacto->getId() . '/update'
    :
    '/contactos/create' ?>"
      method="post" enctype="multipart/form-data">
    <div>
        <label for="nombre">Nombre</label>
        <input type="text" name="nombre" id="nombre"
               value="<?= isset($contacto) ? $contacto->getNombre() : '' ?>">
    </div>
    <div>
        <label for="telefono">Teléfono</label>
        <input type="text" name="telefono" id="telefono"
               value="<?= isset($contacto) ? $contacto->getTelefono() : '' ?>">
    </div>
    <div>
        <label for="foto">foto</label>
        <input type="file" name="foto" id="foto" multiple>
    </div>
    <div>
        <label for="grupo">grupo</label>
        <select name="grupo" id="grupo">
            <?php foreach($grupos as $grupo) : ?>
                <option value="<?= $grupo->getId(); ?>"
                    <?= (isset($contacto) && $contacto->getGrupo() === $grupo->getId())? 'selected' : '' ?>
                >
                    <?= $grupo->getNombre(); ?>
                </option>
            <?php endforeach; ?>
        </select>
    </div>

    <input type="submit" name="enviar" value="Enviar">
</form>