<h2>Contactos</h2>

<section id="search">
    <form id="form-busqueda" action="/" method="POST">
        <input type="search" name="busqueda" value="<?= isset($busqueda) ? $busqueda : '' ?>">
        <input type="submit" name="enviar" value="enviar">
    </form>
</section>

<nav>
    <ul><li><a href="/contactos/new">Crear contacto</a></li></ul>
</nav>

<table>
    <tr>
        <th>Id</th>
        <th>Nombre</th>
        <th>Teléfono</th>
        <th>Grupo</th>
        <th>Fecha alta</th>
        <th>Foto</th>
        <th>Operaciones</th>
    </tr>
    <?php foreach($contactos as $contacto) : ?>
        <tr>
            <td><?= $contacto->getId(); ?></td>
            <td><?= $contacto->getNombre(); ?></td>
            <td><?= $contacto->getTelefono(); ?></td>
            <td><?= $contacto->getGrupo(); ?></td>
            <td><?= $contacto->getFechaAlta(); ?></td>
            <td><img src="<?= $contacto->getFoto(); ?>" alt="Foto contacto" width="100"></td>
            <td>
                <a href="/contactos/<?= $contacto->getId() ?>/edit">Editar</a>
                <a href="/contactos/<?= $contacto->getId() ?>">Show</a>
                <a id="enlace-eliminar-<?= $contacto->getId() ?>" class="enlace-eliminar" href="/contactos/<?= $contacto->getId() ?>">Eliminar</a>
            </td>
        </tr>
    <?php endforeach; ?>
</table>

<p><?php
    $numContactos = count($contactos);
    printf(
        ngettext('Tenemos %d contacto', 'Tenemos %d contactos', $numContactos),
        $numContactos) ?></p>

<script src="js/crud.js"></script>
