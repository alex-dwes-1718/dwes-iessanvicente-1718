<table>
    <tr>
        <th>Id</th>
        <th>Nombre</th>
        <th>Teléfono</th>
        <th>Grupo</th>
        <th>Fecha alta</th>
        <th>Foto</th>
    </tr>
    <tr>
        <td><?= $contacto->getId(); ?></td>
        <td><?= $contacto->getNombre(); ?></td>
        <td><?= $contacto->getTelefono(); ?></td>
        <td><?= $contacto->getGrupo(); ?></td>
        <td><?= $contacto->getFechaAlta(); ?></td>
        <td><img src="/<?= $contacto->getFoto(); ?>" alt="Foto contacto" width="100"></td>
    </tr>
</table>
