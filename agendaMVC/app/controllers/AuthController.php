<?php
/**
 * Created by PhpStorm.
 * User: dwes
 * Date: 17/11/17
 * Time: 18:32
 */

namespace agendaMVC\app\controllers;


use agendaMVC\app\entities\Usuario;
use agendaMVC\core\App;
use agendaMVC\core\Response;
use agendaMVC\core\Security;

class AuthController
{
    public function login()
    {
        $error = null;
        if (isset($_SESSION['error']))
        {
            $error = $_SESSION['error'];
            $_SESSION['error'] = null;
        }
        Response::renderView('login', ['error' => $error]);
    }

    public function checkLogin()
    {
        if (isset($_POST['username']) && !empty($_POST['username']) &&
            isset($_POST['password']) && !empty($_POST['password'])) {
            /**
             * @var Usuario $usuario
             */
            $usuario = App::get('database')->findOneBy(
                'usuarios', 'Usuario',
                [
                    'nombre' => $_POST['username']
                ]);
            if (!is_null($usuario) && Security::checkPassword(
                    $_POST['password'],
                    $usuario->getSalt(),
                    $usuario->getPassword()) === true) {
                $_SESSION['usuario'] = $usuario->getId();
                App::get('router')->redirect('');
            } else {
                $_SESSION['error'] = 'El usuario y password introducidos no existen';

                App::get('router')->redirect('login');
            }
        }
        else
        {
            $_SESSION['error'] = 'Debes introducir el usuario y el password';

            App::get('router')->redirect('login');
        }
    }

    public function logout()
    {
        if (isset ($_SESSION['usuario']))
        {
            $_SESSION['usuario'] = null;
            unset ($_SESSION['usuario']);
        }
        App:: get ('router')->redirect('login');
    }

    public function unauthorized()
    {
        header ('HTTP/1.1 403 Forbidden', true, 403);
        Response:: renderView ('403');
    }
}