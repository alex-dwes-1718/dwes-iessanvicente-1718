<?php

namespace agendaMVC\app\controllers;

use agendaMVC\core\App;
use agendaMVC\core\Response;
use agendaMVC\app\entities\Usuario;
use agendaMVC\core\Security;
use Exception;

class UsuarioController
{
    public function lista()
    {
        if (isset($_POST['busqueda']) && !empty($_POST['busqueda']))
        {
            $busqueda = $_POST['busqueda'];

            $users = App::get('database')->findBy(
                'usuarios', 'Usuario',
                [
                    'nombre'=>$busqueda,
                ], $withLike = true);
        }
        else
        {
            $users = App::get('database')->findAll(
                'usuarios',
                'Usuario'
            );
        }

        Response:: renderView (
            'usuarios',
            [
                'users'=>$users
            ]
        );
    }

    public function show($id)
    {
        $user = App:: get ('database')->findOneBy(
            'usuarios', 'Usuario',
            [
                'id' => $id,
            ]
        );

        Response:: renderView (
            'show-usuario',
            [
                'user'=>$user
            ]
        );
    }

    public function edit($id)
    {
        $user = App:: get ('database')->find('usuarios', 'Usuario', $id);

        Response:: renderView (
            'form-usuario',
            [
                'user'=>$user,
            ]
        );
    }

    public function new()
    {
        Response:: renderView (
            'form-usuario',
            [
            ]
        );
    }

    private function save(Usuario $usuario)
    {
        try
        {
            $parameters = [
                'nombre' => $usuario->getNombre(),
                'password' => $usuario->getPassword(),
                'salt' => $usuario->getSalt()
            ];

            if (is_null($usuario->getId()))
            {
                $parameters['role'] = 'ROLE_USER';
                App::get('database')->insert('usuarios', $parameters);
            }
            else
            {
                $filters = [
                    'id' => $usuario->getId()
                ];
                App::get('database')->update('usuarios', $parameters, $filters);
            }

            App::get('router')->redirect('usuarios');
        }
        catch(\PDOException $pdoException)
        {
            if ($pdoException->getCode() === '23000')
            {
                $_SESSION['error_message'] = 'Ya existe un usuario ' . $usuario->getNombre();
                App::get('router')->redirect('usuarios');
            }
        }
        catch(Exception $exception)
        {
            $error = $exception->getMessage();
        }
    }

    private function validate(Usuario $user)
    {
        if (!isset($_POST['nombre']) || empty(trim($_POST['nombre'])) ||
            !isset($_POST['password']) || empty(trim($_POST['password'])))
        {
            $error = 'No puedes dejar vacío el campo nombre ni el campo password';
            Response:: renderView (
                'form-usuario',
                [
                    'error'=>$error,
                    'user'=>$user,
                ]
            );

            return false;
        }

        return true;
    }

    public function create()
    {
        $user = new Usuario();
        $user->setNombre($_POST['nombre']);

        if ($this->validate($user) === true)
        {
            $salt = Security::getSalt();
            $password = Security::encrypt($_POST['password'], $salt);
            $user->setPassword($password);
            $user->setSalt($salt);
            $this->save($user);
        }
    }

    public function update($id)
    {
        $user = App:: get ('database')->find('usuarios', 'Usuario', $id);
        if (!isset($_POST['nombre']) || empty(trim($_POST['nombre'])))
        {
            $error = 'No puedes dejar vacío el campo nombre';
            Response:: renderView (
                'form-usuario',
                [
                    'error'=>$error,
                    'user'=>$user,
                ]
            );
        }
        else
        {
            $user->setNombre($_POST['nombre']);
            $this->save($user, $_POST['nombre'], $_POST['password']);
        }
    }

    public function delete($id)
    {
        try{
            $filters = [
                'id' => $id,
            ];
            App::get('database')->delete('usuarios', $filters);

            $resultado[] = [
                'code' => '200',
                'message' => 'El usuario ha sido eliminado correctamente'
            ];
            echo json_encode($resultado);
        }
        catch(Exception $exception)
        {
        }
    }
}