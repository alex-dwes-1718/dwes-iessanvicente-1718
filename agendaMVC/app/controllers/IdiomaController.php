<?php
/**
 * Created by PhpStorm.
 * User: dwes
 * Date: 4/12/17
 * Time: 17:35
 */

namespace agendaMVC\app\controllers;


use agendaMVC\core\App;

class IdiomaController
{
    public function idiomaEsp()
    {
        $_SESSION['language'] = 'es_ES.utf8';

        App::get('router')->redirect('');
    }

    public function idiomaEng()
    {
        $_SESSION['language'] = 'en_GB.utf8';

        App::get('router')->redirect('');
    }
}