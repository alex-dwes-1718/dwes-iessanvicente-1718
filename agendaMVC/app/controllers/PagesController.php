<?php

namespace agendaMVC\app\controllers;

use agendaMVC\core\Response;

class PagesController
{
    public function about()
    {
        Response:: renderView (
            'about',
            [
            ]
        );
    }

    public function notFound()
    {
        header ('HTTP/1.1 404 Not Found', true, 404);
        Response:: renderView ('404');
    }
}