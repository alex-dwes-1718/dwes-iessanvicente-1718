<?php

namespace agendaMVC\app\controllers;

use agendaMVC\core\App;
use agendaMVC\core\Response;
use agendaMVC\app\entities\Contacto;
use agendaMVC\app\exceptions\UploadException;
use Exception;

class ContactoController
{
    public function lista()
    {
        $usuario = App::get('user');
        if (isset($_POST['busqueda']) && !empty($_POST['busqueda']))
        {
            $busqueda = $_POST['busqueda'];

            $contactos = App::get('database')->findBy(
                'contactos', 'Contacto',
                [
                    'nombre'=>$busqueda,
                    'usuario'=>$usuario->getId()
                ], $withLike = true);
        }
        else
        {
            $contactos = App::get('database')->findBy(
                'contactos',
                'Contacto',
                ['usuario'=>$usuario->getId()]);
        }

        Response:: renderView (
            'index',
            [
                'contactos'=>$contactos
            ]
        );
    }

    public function show($id)
    {
        $usuario = App::get('user');
        $contacto = App:: get ('database')->findOneBy(
            'contactos', 'Contacto',
            [
                'id' => $id,
                'usuario' => $usuario->getId()
            ]
        );

        Response:: renderView (
            'show-contacto',
            [
                'contacto'=>$contacto
            ]
        );
    }

    public function edit($id)
    {
        $contacto = App:: get ('database')->find('contactos', 'Contacto', $id);
        $grupos = App::get('database')->findAll(
            $table='grupos', $classEntity='Grupo');

        Response:: renderView (
            'form-contacto',
            [
                'contacto'=>$contacto,
                'grupos' => $grupos
            ]
        );
    }

    public function new()
    {
        $grupos = App::get('database')->findAll(
            $table='grupos', $classEntity='Grupo');

        Response:: renderView (
            'form-contacto',
            [
                'grupos' => $grupos
            ]
        );
    }

    private function gestionaImagenContacto(Contacto $contacto)
    {
        try
        {
            $contacto->setDirUpload('uploads');
            $contacto->setNombreCampoFile('foto');
            $contacto->setTiposPermitidos(
                [
                    'image/jpg',
                    'image/jpeg',
                    'image/gif',
                    'image/png'
                ]);
            $contacto->setFoto($contacto->subeImagen());
        }
        catch (UploadException $uploadException)
        {
            if ($uploadException->getFileError() === UPLOAD_ERR_NO_FILE)
                $contacto->setFoto('/uploads/anonimo.png');
            else
            {
                throw $uploadException;
            }
        }
    }

    private function save(Contacto $contacto, $nombre, $telefono, $idGrupo)
    {
        try
        {
            if ($this->validate($contacto) === true)
            {
                $contacto->setNombre($nombre);
                $contacto->setTelefono($telefono);

                $this->gestionaImagenContacto($contacto);

                $grupo = App::get('database')->find(
                    $table='grupos', 'Grupo', $idGrupo);
                $contacto->setGrupo($grupo);

                $parameters = [
                    'nombre' => $contacto->getNombre(),
                    'telefono' => $contacto->getTelefono(),
                    'foto' => $contacto->getFoto(),
                    'grupo' => $contacto->getGrupo()->getId(),
                    'usuario' => App::get('user')->getId()
                ];

                if (is_null($contacto->getId()))
                    App::get('database')->insert('contactos', $parameters);
                else
                {
                    $filters = [
                        'id' => $contacto->getId()
                    ];
                    App::get('database')->update('contactos', $parameters, $filters);
                }

                App::get('router')->redirect('');
            }
        }
        catch(Exception $exception)
        {
            $error = $exception->getMessage();
        }
    }

    private function validate(Contacto $contacto)
    {
        if (!isset($_POST['nombre']) || empty(trim($_POST['nombre'])))
        {
            $_SESSION['error_message'] = 'No puedes dejar vacío el campo nombre';
            $grupos = App::get('database')->findAll(
                $table='grupos', $classEntity='Grupo');
            Response:: renderView (
                'form-contacto',
                [
                    'contacto'=>$contacto,
                    'grupos' => $grupos
                ]
            );

            return false;
        }

        return true;
    }
    public function create()
    {
        $contacto = new Contacto();
        $this->save($contacto, $_POST['nombre'], $_POST['telefono'], $_POST['grupo']);
    }

    public function update($id)
    {
        $contacto = App:: get ('database')->find('contactos', 'Contacto', $id);
        $this->save($contacto, $_POST['nombre'], $_POST['telefono'], $_POST['grupo']);
    }

    public function delete($id)
    {
        try{
            $filters = [
                'id' => $id,
                'usuario' => App::get('user')->getId()
            ];
            App::get('database')->delete('contactos', $filters);

            $resultado[] = [
                'code' => '200',
                'message' => 'El contacto ha sido eliminado correctamente'
            ];
            echo json_encode($resultado);
        }
        catch(Exception $exception)
        {
        }
    }
}