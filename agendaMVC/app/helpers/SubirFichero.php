<?php

namespace agendaMVC\app\helpers;

use agendaMVC\app\exceptions\UploadException;
use Exception;

trait SubirFichero
{
    private $tiposPermitidos;
    private $dirUpload;
    private $nombreCampoFile;
// Movemos el fichero a su nueva ubicación

    /**
     * @return mixed
     */
    public function getTiposPermitidos()
    {
        return $this->tiposPermitidos;
    }

    /**
     * @param mixed $tiposPermitidos
     * @return SubirFichero
     */
    public function setTiposPermitidos($tiposPermitidos)
    {
        $this->tiposPermitidos = $tiposPermitidos;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDirUpload()
    {
        return $this->dirUpload;
    }

    /**
     * @param mixed $dirUpload
     * @return SubirFichero
     */
    public function setDirUpload($dirUpload)
    {
        $this->dirUpload = rtrim($dirUpload, '/');
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNombreCampoFile()
    {
        return $this->nombreCampoFile;
    }

    /**
     * @param mixed $nombreCampoFile
     * @return SubirFichero
     */
    public function setNombreCampoFile($nombreCampoFile)
    {
        $this->nombreCampoFile = $nombreCampoFile;
        return $this;
    }

    private function compruebaTipo()
    {
        $permitido = false;

        for($i = 0;$i < count($this->tiposPermitidos) && $permitido === false;$i++)
        {
            if ($_FILES[$this->nombreCampoFile]['type'] === $this->tiposPermitidos[$i])
                $permitido = true;
        }

        return $permitido;
    }

    private function getNombreImagen()
    {
        $nombre = $this->dirUpload . '/' . $_FILES[$this->nombreCampoFile]['name'];

        if (is_file($nombre) === true)
        {
            $idUnico = time();
            $nombre = $this->dirUpload . '/' . $idUnico.'_' . $_FILES[$this->nombreCampoFile]['name'];
        }

        return $nombre;
    }

    private function muestraImagen(string $nombreImagen)
    {
        header('Content-type: ' . $_FILES[$this->nombreCampoFile]['type']);

        $fp = fopen($nombreImagen, 'rb');
        $contenido = fread ($fp, filesize ($nombreImagen));
        fclose ($fp);

        echo $contenido;
    }

    public function subeImagen($devolverImagen = false)
    {
        if ($_FILES[$this->nombreCampoFile]['error'] !== UPLOAD_ERR_OK)
            throw new UploadException(
                $_FILES[$this->nombreCampoFile]['error']);

        $permitido = $this->compruebaTipo();
        if ($permitido === false)
            throw new Exception(
        'Error: No se trata de un fichero .GIF.');

        if (is_uploaded_file($_FILES[$this->nombreCampoFile]['tmp_name']) === false)
            throw new Exception(
        'Error: posible ataque. Nombre: '.$_FILES['imagen']['name']);

        $nombreImagen = $this->getNombreImagen();

        if (move_uploaded_file($_FILES[$this->nombreCampoFile]['tmp_name'], $nombreImagen))
        {
            if ($devolverImagen === true)
                $this->muestraImagen($nombreImagen);
        }
        else
            throw new Exception(
                'Error: No se puede mover el fichero a su destino');

        return $nombreImagen;
    }
}