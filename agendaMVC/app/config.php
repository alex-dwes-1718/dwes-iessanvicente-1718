<?php

return array(
    'database' => array(
        'name' => 'agenda',
        'username' => 'usuAgenda',
        'password' => 'dwes',
        'connection' => 'mysql:host=dwes.local',
        'options' => array(
            PDO:: MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            PDO:: ATTR_ERRMODE => PDO:: ERRMODE_EXCEPTION ,
            PDO:: ATTR_PERSISTENT => true
        )
    ),
    'logs' => array(
        'name' => 'Registro Error',
        'file' => '../logs/agenda-error.log'
    ),
    'security' => array(
        'roles' => array(
            'ROLE_ADMIN'=>3,
            'ROLE_USER'=>2,
            'ROLE_ANONIMO'=>1
        )
    )
);