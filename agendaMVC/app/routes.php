<?php
/**
 * @var \agendaMVC\core\Router $router
 */

$router->get('', 'ContactoController@lista', 'ROLE_USER');
$router->post('', 'ContactoController@lista', 'ROLE_USER');

$router->get('about', 'PagesController@about');

$router->get(
    'contactos/new', 'ContactoController@new', 'ROLE_USER');

$router->get('contactos/:id', 'ContactoController@show', 'ROLE_USER');

$router->get('contactos/:id/edit', 'ContactoController@edit', 'ROLE_USER');

$router->post('contactos/:id/update', 'ContactoController@update', 'ROLE_USER');

$router->post(
    'contactos/create',
    'ContactoController@create', 'ROLE_USER');

$router->delete('contactos/:id', 'ContactoController@delete', 'ROLE_USER');

$router->get('login', 'AuthController@login');

$router->post('check-login', 'AuthController@checkLogin');

$router->get('logout', 'AuthController@logout', 'ROLE_USER');

$router->get('usuarios', 'UsuarioController@lista', 'ROLE_ADMIN');
$router->post('usuarios', 'UsuarioController@lista', 'ROLE_ADMIN');
$router->get(
    'usuarios/new', 'UsuarioController@new', 'ROLE_ADMIN');

$router->get('usuarios/:id', 'UsuarioController@show', 'ROLE_ADMIN');

$router->get('usuarios/:id/edit', 'UsuarioController@edit', 'ROLE_ADMIN');

$router->post('usuarios/:id/update', 'UsuarioController@update', 'ROLE_ADMIN');

$router->post(
    'usuarios/create',
    'UsuarioController@create', 'ROLE_ADMIN');

$router->delete('usuarios/:id', 'UsuarioController@delete', 'ROLE_ADMIN');

$router->get('idiomas/es', 'IdiomaController@idiomaEsp');

$router->get('idiomas/en', 'IdiomaController@idiomaEng');