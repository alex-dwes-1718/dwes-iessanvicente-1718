<?php

namespace agendaMVC\core;

class Response
{
    public static function renderView($name, $data = array())
    {
        extract ($data);

        if (isset($_SESSION['error_message']))
        {
            $error_message = $_SESSION['error_message'];
            $_SESSION['error_message'] = null;
        }

        $usuario = App:: get ('user');

        ob_start ();

        require __DIR__ . "/../app/views/$name.view.php";

        $mainContent = ob_get_clean ();

        require __DIR__ . '/../app/views/layout.view.php';
    }
}