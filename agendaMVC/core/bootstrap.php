<?php

use agendaMVC\core\App;
use agendaMVC\core\database\Connection;
use agendaMVC\core\database\QueryBuilder;

session_start();

$config = require __DIR__ . '/../app/config.php';
App:: bind ('config', $config);

App:: bind ('database', new QueryBuilder(
    Connection:: make ($config['database'])
));