<?php

require_once 'Agenda.class.php';
require_once __DIR__ . '/entities/contacto.class.php';
require_once 'database/Connection.class.php';
require_once __DIR__ . '/database/QueryBuilder.class.php';

$pdo = Connection::make();

$qb = new QueryBuilder($pdo);

if ($_SERVER['REQUEST_METHOD'] === 'POST' && !isset($_POST['btBuscar']))
{
    try
    {

        $contacto = new Contacto();
        $contacto->setNombre($_POST['nombre']);
        $contacto->setTelefono($_POST['telefono']);

        $contacto->setDirUpload('./uploads');
        $contacto->setNombreCampoFile('foto');
        $contacto->setTiposPermitidos(
            [
                'image/jpg',
                'image/jpeg',
                'image/gif',
                'image/png'
            ]);
        $contacto->setFoto($contacto->subeImagen());

        $grupo = Agenda::findGrupoById($pdo, $_POST['grupo']);
        $contacto->setGrupo($grupo);

        Agenda::addContacto($pdo, $contacto);
    }
    catch (UploadException $uploadException)
    {
        $error = $uploadException->getMessage();
    }
    catch(Exception $exception)
    {
        $error = $exception->getMessage();
    }
}
elseif(isset($_GET['operacion']) && $_GET['operacion'] === 'borrar')
{
    if (isset($_GET['id']) && !empty($_GET['id']))
        Agenda::deleteContacto($pdo, $_GET['id']);
}

$grupos = Agenda::getGrupos($qb);
if (isset($_POST['btBuscar']) && $_POST['btBuscar'] === 'buscar')
    $contactos = Agenda::findByNombre($pdo, $_POST['buscar']);
else
    $contactos = Agenda::getContactos($qb);

include 'index.view.php';