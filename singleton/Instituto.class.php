<?php

require_once 'Singleton.trait.php';

class Instituto
{
    use Singleton;

    private $direccion;

    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    }

    public function getDireccion()
    {
        return $this->direccion;
    }
}