<?php

require_once 'Singleton.trait.php';

class Empresa
{
    use Singleton;

    private $nombre;

    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    public function getNombre()
    {
        return $this->nombre;
    }
}